#!/bin/sh
cd "$(dirname "$0")" || exit 1
scheme="$HOME/.local/share/flavours/base16/schemes/horizon/horizon-dark.yaml"
flavours build\
	$scheme\
	themed-css/style.mustache\
	> assets/css/style.css

flavours build\
	$scheme\
	themed-svg/rss.svg\
	> assets/icons/rss.svg

flavours build\
	$scheme\
	themed-svg/calendar.svg\
	> assets/icons/calendar.svg
